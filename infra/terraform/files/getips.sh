#! /bin/bash
text_file_node="\n- targets:\n"
text_file_cadvisor="$text_file_node"
text_file_go="$text_file_node"
ids=""
REGION="$2"
output_dir=../ansible/roles/cloudalchemy.prometheus/targets/
output_node=node.yml
output_cadvisor=cadvisor.yml
output_go=go.yml
cadvisor_port="9280"
node_port="9100"
go_port="8080"

current_ip=$(aws ec2 describe-instances --region $REGION  --filters Name=tag:Name,Values="Control Node Server ($1)" --query Reservations[*].Instances[].PrivateIpAddress --output text)

text_file_node="$text_file_node  - $current_ip:$node_port\n  labels:\n    job: control_node\n\n- targets:\n  - localhost:$node_port\n  labels:\n    job: monitoring_server\n\n- targets:\n"
text_file_cadvisor="$text_file_cadvisor  - $current_ip:$cadvisor_port\n  labels:\n    job: control_node\n\n- targets:\n"

current_ip=$(aws ec2 describe-instances --region $REGION --filters Name=tag:Name,Values="GO Server ($1)" --query Reservations[*].Instances[].PrivateIpAddress --output text)

text_file_node="$text_file_node  - $current_ip:$node_port\n  labels:\n    job: elb\n\n- targets:\n"

while [ "$ids" = "" ]; do
  ids=$(aws autoscaling describe-auto-scaling-groups --filters Name=tag:Name,Values="Docker-ASG-$1" --region $REGION --query AutoScalingGroups[].Instances[].InstanceId --output text)
  sleep 1
done
for ID in $ids;
do
    IP=$(aws ec2 describe-instances --instance-ids $ID --region $REGION --query Reservations[].Instances[].PrivateIpAddress --output text)
    text_file_node="$text_file_node  - $IP:$node_port\n"
    text_file_cadvisor="$text_file_cadvisor  - $IP:$cadvisor_port\n"
    text_file_go="$text_file_go  - $IP:$go_port\n"
done

text_file_node="$text_file_node  labels:\n    job: asg\n"
text_file_cadvisor="$text_file_cadvisor  labels:\n    job: asg\n"
text_file_go="$text_file_go  labels:\n    job: asg\n"
rm -fr $output_dir
mkdir $output_dir
printf "$text_file_node" > $output_dir$output_node
printf "$text_file_cadvisor" > $output_dir$output_cadvisor
printf "$text_file_go" > $output_dir$output_go
 
