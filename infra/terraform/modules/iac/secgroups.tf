# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "web" {
  name = "Dynamic Security Group for Web (${terraform.workspace})"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${var.ec2_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for Application (${terraform.workspace})"
  }
}


# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "elb" {
  name = "Dynamic Security Group for ELB (${terraform.workspace})"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${var.elb_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for ELB (${terraform.workspace})"
  }
}

# Созаём правило, которое будет разрешать трафик к нашему узлу управления
resource "aws_security_group" "control_node" {
  name = "Dynamic Security Group for control node (${terraform.workspace})"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = "${var.cn_ports}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Web access for control node (${terraform.workspace})"
  }
}

resource "aws_security_group" "mon_server" {
  name        = "Monitoring Security Group (${terraform.workspace})"

  dynamic "ingress" {
    for_each = "${var.mon_ports_tcp}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  dynamic "ingress" {
    for_each = "${var.mon_ports_udp}"
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = "${var.allowed_cidr_blocks}"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = "${var.allowed_cidr_blocks}"
  }

  tags = {
    Name  = "Monitoring Server Security Group (${terraform.workspace})"
  }
}
