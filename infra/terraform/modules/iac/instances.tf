
# Запускаем инстанс control_node
resource "aws_instance" "my_cn" {
  # с выбранным образом ubuntu 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  iam_instance_profile = "${aws_iam_instance_profile.ec2_profile.name}"
  vpc_security_group_ids = [aws_security_group.control_node.id]
  key_name = "${var.aws_keyname}"

  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Control Node Server (${terraform.workspace})"
  }

  connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
  }

  provisioner "file" {
    content = <<-EOT
      os_endpoint: ${aws_opensearch_domain.skill_study.endpoint}
      os_user: ${var.os_user}
      os_pass: ${var.os_pass}
      aws_region: ${var.aws_region}
    EOT
    destination = "/tmp/os.yml"
  }

  provisioner "remote-exec" {
    inline = ["sudo apt update -y",
              "sudo apt-get install python3 -y",
              "sudo mkdir /var/www",
              "sudo cp /tmp/os.yml /var/www/os.yml",
              "sudo chmod 400 /var/www/os.yml"]
  }

  provisioner "local-exec" {
    command = <<-EOT
      files/remove_runners.sh '${var.gitlab_access_api_token}'
      rm -rf '${var.def_path}'
      mkdir '${var.def_path}'
      echo 'runner_tags: aws,docker,${terraform.workspace}' >> '${var.def_path}/cn.yml'
      echo 'os_endpoint: ${aws_opensearch_domain.skill_study.endpoint}' >> '${var.def_path}/os.yml'
      echo 'os_user: ${var.os_user}' >> '${var.def_path}/os.yml'
      echo 'os_pass: ${var.os_pass}' >> '${var.def_path}/os.yml'
      echo 'aws_region: ${var.aws_region}' >> '${var.def_path}/os.yml'
      echo 'grafana_user: ${var.grafana_user}' >> '${var.def_path}/monitoring.yml'
      echo 'grafana_password: ${var.grafana_pass}' >> '${var.def_path}/monitoring.yml'
      echo 'gitlab_token: ${var.gitlab_token}' >> '${var.def_path}/cn.yml'
      echo 'pagerduty_user_url: ${var.pagerduty_user_url}' >> '${var.def_path}/monitoring.yml'
      echo 'pagerduty_user_key: ${var.pagerduty_user_key}' >> '${var.def_path}/monitoring.yml'
      ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_key_private} ../ansible/control-node/main.yml -b -v
    EOT
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Запускаем инстанс ELB
resource "aws_instance" "my_webserver" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.elb.id]
  key_name = "${var.aws_keyname}"
  tags = {
    Name  = "GO Server (${terraform.workspace})"
    Tier = "Backend"
    CM = "Ansible"
  }

  depends_on = [aws_instance.my_cn]

  connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
  }  

  provisioner "remote-exec" {
    inline = ["sudo apt update -y",
              "sudo apt-get install python3 -y"]
  }

  provisioner "local-exec" {
    command = <<-EOT
      ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_key_private} ../ansible/elb/main.yml -b -v
    EOT
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Запускаем Prometheus сервер
resource "aws_instance" "mon_server" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.mon_server.id]
  key_name = "${var.aws_keyname}"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Monitoring Server (${terraform.workspace})"
  }

  depends_on = [aws_launch_configuration.web, aws_instance.my_cn, aws_instance.my_webserver]

  provisioner "remote-exec" {
    inline = ["sudo apt update -y",
              "sudo apt-get install python3 -y"]

    connection {
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file(var.ssh_key_private)}"
    }
  }

  provisioner "local-exec" {
    command = <<-EOT
      echo 'env_domain: ${local.env_domain}' >> ../ansible/monitoring/env_domain.yml
      ./files/getips.sh ${terraform.workspace} ${var.aws_region}
      ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key ${var.ssh_key_private} ../ansible/monitoring/main.yml --extra-vars 'input_domain=${var.route53_hosted_zone_name}' -b -v
      rm ../ansible/monitoring/env_domain.yml
      rm -rf '${var.def_path}'
    EOT
  }

}



