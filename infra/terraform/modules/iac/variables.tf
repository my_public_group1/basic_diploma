variable "route53_hosted_zone_name" {
  type = string
}

variable "ssh_key_private" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "allowed_cidr_blocks" {
  type = list
  default = ["0.0.0.0/0"]
}

variable "elb_ports" {
  type = list
  default = ["22", "80", "443", "9100"]
}

variable "ec2_ports" {
  type = list
  default = ["22", "80", "8080", "9100", "9280"]
}

variable "aws_keyname" {
  type = string
}

variable "cn_ports" {
  type = list
  default = ["22", "9100", "9280"]
}

variable "mon_ports_udp" {
  type = list
  default = ["9094"]
}

variable "mon_ports_tcp" {
  type = list
  default = ["9090", "3000", "9093", "22", "9100", "9094", "80"]
}

variable "os_user" {
  type = string
}

variable "os_pass" {
  type = string
}

variable "def_path" {
  type = string
  default = "../ansible/general_defaults"
}

variable "def_server_path" {
  type = string
  default = "/var/www"
}

variable "grafana_user" {
  type = string
}

variable "grafana_pass" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "gitlab_access_api_token" {
  type = string
}

variable "pagerduty_user_url" {
  type = string
}

variable "pagerduty_user_key" {
  type = string
}

