# Docker-GO-diploma

[![Build Status](https://gitlab.com/my_public_group1/basic_diploma/badges/main/pipeline.svg)](https://gitlab.com/my_public_group1/basic_diploma)

Данный проект является дипломной работой курса "DevOps-инженер. Основы" и содержит в себе следующие этапы:

- Создание инфраструктуры в облаке Amazon AWS с помощью инструмента Terraform;
- Конфигурация инфраструктуры и deploy приложения с помощью Ansible и Docker;
- Тестирование приложения и запуск плейбука Ansible с помощью Gitlab CI/CD.

## Требования для запуска проекта:

1. Зарегистрируйте аккаунт [Amazon AWS](https://aws.amazon.com/account/sign-up).
2. Установите [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) и [сконфигурируйте его](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) для управления аккаунтом и ресурсами.
3. Создайте [SSH-ключ](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/create-key-pairs.html) и скопируйте его к себе.
4. Купить домен у любого регистратора или зарегистрировать бесплатный, [создать hosted zone в AWS Route 53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html) и [прописать NS-сервера у регистратора домена](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-name-servers-glue-records.html).
5. Зарегистрируйте аккаунт [PagerDuty](https://www.pagerduty.com/sign-up/), создайте сервис получения уведомлений с интеграцией Prometheus, и сохраните у себя url и приватный ключ.
6. Установите [Terraform](https://www.terraform.io/downloads)
7. Установите [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Описание инфраструктуры проекта:

Инфраструктура проекта предусматривает создание следующих элементов: 

| Элемент | Назначение |
| --- | ----------- |
| [AWS ELB Server](https://aws.amazon.com/elasticloadbalancing/) | Необходим для балансировки нагрузки между серверами приложения |
| [AWS Auto Scaling Group](https://aws.amazon.com/autoscaling/) | Группа автоматического масштабирования серверов приложения в зависимости от нагрузки | 
| Monitoring Server | Необходим для мониторинга всех серверов инфраструктуры |
| Control Node Server | Используется для работы gitlab-runner, а также для динамического создания inventory файла |
| [AWS Route53](https://aws.amazon.com/route53/) Records | DNS записи для приложения и инструментов мониторинга в системе Route53 |
| [AWS OpenSearch](https://aws.amazon.com/opensearch-service/) | Аналитическая система, используемая для сбора и анализа различных журналов инфраструктуры |

> Таблица 1.

Схема взаимодействия между элементами инфраструктуры представлена ниже:

![scheme](images/aws-iac.png)

Инфраструктура создается следующим образом:

1. Создаётся AWS Elastic Load Balancer для балансировки нагрузки между инстансами автоматической группы масштабирования AWS Auto Scaling Group;

2. Создаются инстансы AWS AUto Scaling Group для работы приложения GO;

3. Создаётся сервер Control Node, который в свою очередь конфигурирует и запускает gitlab-runner, динамически формирует inventory файл Ansible с инстансами AWS Auto Scaling Group, а также запускает Ansible Playbook на эти инстансы;

4. Создается сервер мониторинга, который собирает метрики с инстансов AWS Auto Scaling Group, c Control Node сервера, ELB сервера, а также с самого себя;

5. Создается домен AWS OpenSearch, который собирает журналы с инстансов AWS Auto Scaling Group, c Control Node сервера, ELB сервера, а также с сервера мониторинга. Отправка журналов с серверов происходит с помощью иструмента [fluent-bit](https://fluentbit.io/);

6. С помощью AWS Route53 создаются DNS записи для самого приложения, а также для сервисов мониторинга и аналитики логов, причем разные для тестового и продакшн окружений.

## Как работать с gitlab:

Необходимо сохранить у себя токен для создания gitlab-runner, для этого выполните следующие действия:

1. Перейдите `Settings -> CI/CD -> Runners`;

2. В области `Shared Runners` отключите флажок `Enable shared runners for this project`;

3. В области `Specific Runners` найдите строку `And this registration token` и сохраните у себя токен, который следует за этой строкой. Этот токен необходим для создания новых gitlab-раннеров.

4.  Далее необходимо перейти в общие настройки gitlab. Для этого необходимо нажать на аватар профиля справа сверху и выбрать `Preferences`. На открывшейся странице необходимо выбрать вкладку `Access Token`. Далее создаем новый токен, выбираем для него любое имя и права доступа `api`, `read_api`. После создания появится токен `Your new personal access token`, его необходимо скопировать себе и сохранить. Этот токен необходим для запуска скрипта удаления не активных gitlab-раннеров.

## Как работать с проектом:
1. Скопируйте SSH-ключ для управления AWS в директорию /var/www:

   ```shell
   cp /path/to/your/key /var/www/
   ```

2. Склонируйте репозиторий к себе для работы с ним:

   ```shell
   git clone https://gitlab.com/my_public_group1/basic_diploma
   ```

3. Перейдите в директорию `infra/terraform` и переименуйте файл `main.tf.example` в `main.tf`, после отредактриуйте следующие переменные:

| Переменная | Значение |
| --- | ----------- |
| route53_hosted_zone_name | Ваш домен, зона которого была сконфигрурирована в Route 53 |
| aws_region | Регион Amazon AWS, в котором необходимо создавать ресурсы |
| aws_keyname | название SSH-ключа для управления ресурсами AWS без формата файла |
| ssh_key_private | Полный путь до SSH-ключа для управления ресурсами AWS с полным названием файла |
| os_user | Имя пользователя для входа в OpenSearch |
| os_pass | Пароль для входа в OpenSearch |
| grafana_user | Имя пользователя для входа в grafana |
| grafana_pass | Пароль для входа в grafana |
| gitlab_token | Токен для создания gitlab-раннера |
| gitlab_access_api_token | Токен для доступа к API gitlab |
| pagerduty_user_url | PagerDuty URL |
| pagerduty_user_key | PagerDuty Key |

> Таблица 2.

4. Инициализируйте Terraform и создайте два рабочих пространства для тестового и продакшн окружений:

   ```shell
   terraform init 
   terraform workspace new test
   terraform workspace new app
   ```

5. Выберите рабочие пространство для тестового окружения и запустите создание ресурсов AWS:

   ```shell
   terraform workspace select test
   terraform apply
   ```

6. Далее необходимо выполнить аналогичные действия для создания продакшн окружения:

   ```shell
   terraform workspace select app
   terraform apply
   ```
7. После создания ресурсов через `Terraform` создайте тестовую ветку с названием `uat-ddmmyy` выполните `commit` и `push` наших изменений на `gitlab.com` , что вызовет запуск `ansible playbook` для развертывания `docker` контейнеров с нашим `GO` приложением на тестовое окружение:

   ```shell
   git checkout -b uat-ddmmyy
   git add .
   git commit -m "init_test_commit"
   git push --set-upstream origin uat-221022
   ```

8. Далее, если в тестовом окружении все выполнилось без ошибок, и поднялось наше приложение, необходимо создать запрос на слияние (merge request) тестовой ветки и основной `main` на `gitlab.com`, где содержится код приложения. После подверждения данного запроса будет выполнен запуск ansible playbook для развертывания `docker` контейнеров с нашим `GO` приложением на продакшн окружение.

9. Результат выполнения можно увидеть после выполнения pipeline по следующим субдоменам `(http и https доступны)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | app.example.com |
| test | test.example.com |

> Таблица 3.

10. Мониторинг доступен по различным адресам, в зависимости от окружения `(доступен только http)`:

| Окружение | Инструмент мониторинга | Субдомен |
| --- | ----------- | ----------- |
| app | Prometheus | prom.example.com |
|| Grafana | grafana.example.com |
|| Alertmanager | am.example.com |
| test | Prometheus | prom-test.example.com |
|| Grafana | grafana-test.example.com |
|| Alertmanager | am-test.example.com |

> Таблица 4.

11. Дэшборд OpenSearch доступны по следующим доменами `(https доступен)`:

| Окружение | Субдомен |
| --- | ----------- |
| app | os.example.com/_dashboards |
| test | os-test.example.com/_dashboards |

> Таблица 5.
